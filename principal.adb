with Ada.Text_IO,Ada.Real_Time,Ada.Float_Text_Io,Sensor,Calefactor,PID;
use Ada.Text_IO,Ada.Real_Time,Ada.Float_Text_Io,Sensor,Calefactor;
   procedure Principal is

     package Control_Horno is new PID(Real=>Float,Entrada=>Temperaturas,Salida=>Potencias); --Instancia PID
     package Temperatura_ES is new Ada.Text_Io.Float_Io(Temperaturas);                      --E/S tipo FLOAT
     package Tiempo_Periodo is new Ada.Text_Io.Float_Io(Float);                             --E/S impresion Tiempo_Muestra
     use Control_Horno;
     Controlador_del_Horno:Controlador;

     Ts:constant:=1.0; --Periodo de la muestra
     Temperatura_Actual:Temperaturas;
     Temperatura_Referencia_1:Temperaturas:=100.00;
     Temperatura_Referencia_2:Temperaturas:=200.00;
     Potencia_Horno:Potencias;
     Ct:constant:=1_500.0;
     Cp:constant:=25.0;
     L:constant:=1.5;
     Kp,Ki,Kd:float;
     R:float:=float(Temperatura_Referencia_2-Cp)/(Ct/Cp);

     --Declaramos el Periodo de muestreo
     Siguiente_instante:Time:=Clock;
     Tiempo_Muestra: Float:= Ts;
     Periodo: constant Time_Span:=To_Time_Span(Ts);

   begin
     --Declaramos constantes PID
     Kp:= 1.2/(R*L);
     Ki:= Kp/(2.0*L)*Ts;
     Kd:= Kp*(0.56*L) *Ts;
     --Programamos el horno con nuestras constantes
     Control_Horno.Programar(Controlador_del_Horno,Kp,Ki,Kd);

     bucle : loop --Bucle infinito
     delay until Siguiente_instante;  --Esperamos hasta que pase Ts segundos
       Leer(Temperatura_Actual);      --Leemos temperatura actual
       Control_Horno.Controlar(Controlador_del_Horno,Temperatura_Referencia_2,Temperatura_Actual,Potencia_Horno); --Mandamos parámetros al controlador
       Tiempo_Muestra:= Tiempo_Muestra+Ts;  --Cogemos el nuevo periodo de muestra
       Tiempo_Periodo.Put(Item=>Tiempo_Muestra, Aft=>2, Exp=>0);  --Imprimimos tiempo muestra
       put(" ");
       Temperatura_ES.Put(Item=>Temperatura_Actual,Aft=>2,Exp=>0);      --Imprimimos temperatura actual
       New_Line;
       Escribir(Potencia_Horno);                                  --Mandamos potencia que nos ha devuelto el controlador al horno
       Siguiente_instante:= (Siguiente_instante+Periodo);         --Cálculamos siguiente instante
       exit bucle  when Tiempo_Muestra = 600.0;                   --Salimos del bucle a los 10 minutos
     end loop bucle;

     Escribir(0.0); --Apagamos el horno
  end Principal;
